package com.drive.desktop.core;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.api.client.http.FileContent;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;

public class DriveApp {

	private Drive drive;

	public DriveApp() {
		try {
			this.drive = DriveConnection.getService();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Iterable<File> getChildrenOf(String parent) {
		try {
			FileList result = drive.files().list().setQ("'" + parent + "' in parents and trashed = false")
					.setFields("files(id, name, parents, mimeType)").execute();
			return result.getFiles();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void upload(File file) {
		try {
			drive.files().create(file).execute();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Drive getDriveService() {
		return this.drive;
	}
	
	public static void main(String[] args) {
		DriveApp drive = new DriveApp();
		File fileMetadata = new File();
		fileMetadata.setName("My Report");

		java.io.File filePath = new java.io.File("D:/Documents/text.txt");
		FileContent mediaContent = new FileContent("text/text", filePath);
		File file;
		try {
			file = drive.getDriveService().files().create(fileMetadata, mediaContent)
			.setFields("id")
			.execute();
			System.out.println("File ID: " + file.getId());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
}
