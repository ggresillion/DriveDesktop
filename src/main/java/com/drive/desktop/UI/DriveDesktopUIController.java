package com.drive.desktop.UI;

import com.drive.desktop.UI.model.DriveTreeItem;
import com.drive.desktop.core.DriveApp;
import com.google.api.services.drive.model.File;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.TreeView;
import javafx.scene.layout.VBox;

public class DriveDesktopUIController {

	@FXML
	private TreeView<String> treeView;
	private DriveApp drive;

	@FXML
	public void initialize() {
		this.drive = new DriveApp();

		treeView.getSelectionModel().selectedItemProperty()
				.addListener((observable, oldValue, newValue) -> onExpand((DriveTreeItem) newValue));
	}

	private Object onExpand(DriveTreeItem newValue) {
		if (newValue.isDirectory()) {
			if (newValue.isExpanded()) {
				newValue.setExpanded(false);
			} else if (newValue.getChildren().isEmpty()) {
				newValue.loadChildren();
				newValue.setExpanded(true);
			} else {
				newValue.setExpanded(true);
			}

		}
		return null;
	}

	public void createContent() {
		VBox treeBox = new VBox();
		treeBox.setPadding(new Insets(10, 10, 10, 10));
		treeBox.setSpacing(10);

		File rootFile = new File();
		rootFile.setName("Root");

		createFileTree();
	}

	private void createFileTree() {
		DriveTreeItem rootNode = new DriveTreeItem(drive);
		treeView.setRoot(rootNode);
	}
}
