package com.drive.desktop.UI.model;

import com.drive.desktop.core.DriveApp;
import com.google.api.services.drive.model.File;

import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class DriveTreeItem extends TreeItem<String> {

	public static Image folderCollapseImage = new Image(ClassLoader.getSystemResourceAsStream("Folder.png"));
	public static Image folderExpandImage = new Image(ClassLoader.getSystemResourceAsStream("Folder.png"));
	public static Image fileImage = new Image(ClassLoader.getSystemResourceAsStream("Text-x-generic.png"));

	private File file;
	private DriveApp drive;

	private DriveTreeItem(DriveApp drive, File file) {
		super(file.getName());
		this.drive = drive;
		this.file = file;

		if (file.getMimeType().equals("application/vnd.google-apps.folder")) {
			this.setGraphic(new ImageView(new Image(ClassLoader.getSystemResourceAsStream("Folder.png"))));
		} else {
			this.setGraphic(new ImageView(new Image(ClassLoader.getSystemResourceAsStream("Text-x-generic.png"))));
		}
		// loadChildren(file);
	}

	public DriveTreeItem(DriveApp drive) {
		super("My Drive");
		this.drive = drive;
		loadRootChildren();
		this.setGraphic(new ImageView(new Image(ClassLoader.getSystemResourceAsStream("logo-drive.png"))));
	}

	private void loadRootChildren() {
		Iterable<File> files = drive.getChildrenOf("root");
		for (File file : files) {
			this.getChildren().add(new DriveTreeItem(drive, file));
		}
	}

	public void loadChildren() {
		if (file.getMimeType().equals("application/vnd.google-apps.folder")) {
			Iterable<File> files = drive.getChildrenOf(file.getId());
			for (File childFile : files) {
				this.getChildren().add(new DriveTreeItem(drive, childFile));
			}
		}
	}

	public boolean isDirectory() {
		if (this.file.getMimeType().equals("application/vnd.google-apps.folder")) {
			return true;
		}
		return false;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

}
