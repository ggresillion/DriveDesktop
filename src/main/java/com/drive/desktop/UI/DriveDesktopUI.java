package com.drive.desktop.UI;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class DriveDesktopUI extends Application {

	private Stage primaryStage;
	private FXMLLoader loader;

	@Override
	public void start(Stage primaryStage) throws Exception {
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("Browse Google Drive");
		try {
			loader = new FXMLLoader(DriveDesktopUI.class.getResource("DriveDesktopUI.fxml"));
			BorderPane rootLayout = (BorderPane) loader.load();
			Scene scene = new Scene(rootLayout);
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}

		createContent();
	}

	private void createContent() {
		DriveDesktopUIController controller = loader.getController();
		controller.createContent();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
